class AddNicknameToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :nickname, :string, after: :header_id
  end
end
