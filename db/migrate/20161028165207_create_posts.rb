class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :header_id
      t.string  :city
      t.text    :text
      t.text    :image
      t.timestamps
    end
  end
end
