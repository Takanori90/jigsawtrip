Rails.application.routes.draw do
  devise_for :users
  root 'posts#index'                     # ルートパスの指定
  get  'posts'      =>   'posts#index'   # ツイート一覧画面
  get  'posts/new'  =>   'posts#new'     # ツイート投稿画面
  post 'posts'      =>   'posts#create'  # ツイート投稿機能
end
